// ignore_for_file: prefer_const_constructors, use_key_in_widget_constructors

import 'package:flutter/material.dart';

class ThirdPage extends StatefulWidget {
  final String name;

  ThirdPage({required this.name});

  @override
  State<ThirdPage> createState() => _ThirdPageState();
}

class _ThirdPageState extends State<ThirdPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Seed of Hope Signup Page'),
      ),
      body: Center(
        child: Text('Hi ${widget.name}.Fill in your details to register'),
      ),
    );
  }
}
