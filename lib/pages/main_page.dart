// ignore_for_file: prefer_const_constructors

import 'package:finalquestion3/routes/routes.dart';
import 'package:flutter/material.dart';

class MainPage extends StatefulWidget {
  const MainPage({super.key});

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Seed of Hope'), centerTitle: null),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              child: Text("Sign In"),
              onPressed: () {
                Navigator.of(context).pushNamed(RouteManager.secondPage);
              },
            ),
            SizedBox(
              height: 5,
            ),
            ElevatedButton(
              child: Text('Sign Up'),
              onPressed: () {
                Navigator.of(context)
                    .pushNamed(RouteManager.thirdpage, arguments: {
                  'name': 'New User',
                });
              },
            ),
          ],
        ),
      ),
    );
  }
}
