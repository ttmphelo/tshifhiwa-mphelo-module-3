import 'dart:js';

import 'package:finalquestion3/pages/fourth_page.dart';
import 'package:finalquestion3/pages/second_page.dart';
import 'package:finalquestion3/pages/third_page.dart';
import 'package:flutter/material.dart';

import '../pages/main_page.dart';

class RouteManager {
  static const String homePage = '/';
  static const String secondPage = '/secondPage';
  static const String thirdpage = '/thirdPage';
  static const String fourthpage = '/fourthPage';

  static Route<dynamic> generateRoute(RouteSettings settings) {
    var valuePassed;

    if (settings.arguments != null) {
      valuePassed = settings.arguments as Map<String, dynamic>;
    }
    switch (settings.name) {
      case homePage:
        return MaterialPageRoute(
          builder: (context) => MainPage(),
        );

      case secondPage:
        return MaterialPageRoute(
          builder: (context) => SecondPage(),
        );

      case fourthpage:
        return MaterialPageRoute(
          builder: (context) => FourthPage(),
        );

      case thirdpage:
        return MaterialPageRoute(
          builder: (context) => ThirdPage(
            name: valuePassed['name'],
          ),
        );

      default:
        throw FormatException('Route not found! check routes again');
    }
  }
}
